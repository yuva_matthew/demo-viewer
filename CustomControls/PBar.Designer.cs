﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace CustomControls {
    partial class PBar {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent() {
            this.percent = new Label();
            this.SuspendLayout();
            // 
            // percent
            // 
            this.percent.AutoSize = true;
            this.percent.BackColor = Color.Transparent;
            this.percent.Font = new Font("Segoe UI", 9F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(204)));
            this.percent.Location = new Point(62, 5);
            this.percent.Margin = new Padding(0);
            this.percent.Name = "percent";
            this.percent.Size = new Size(26, 15);
            this.percent.TabIndex = 0;
            this.percent.Text = "0 %";
            this.percent.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // PBar
            // 
            this.AutoScaleDimensions = new SizeF(6F, 13F);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.BackColor = SystemColors.Window;
            this.Controls.Add(this.percent);
            this.DoubleBuffered = true;
            this.Name = "PBar";
            this.Size = new Size(150, 25);
            this.SizeChanged += new EventHandler(this.PBar_SizeChanged);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label percent;
    }
}

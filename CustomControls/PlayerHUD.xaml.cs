﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using DemoInfo;

namespace CustomControls
{
    /// <summary>
    /// Логика взаимодействия для PlayerHUD.xaml
    /// </summary>
    public partial class PlayerHUD : Control {
        public PlayerHUD() {
            InitializeComponent();
        }

		#region HealthFontFamily 

		public FontFamily HealthFontFamily {
			get { return (FontFamily)GetValue(HealthFontFamilyProperty); }
			set { SetValue(HealthFontFamilyProperty, value); }
		}

		// Using a DependencyProperty as the backing store for HealthFontFamily.  This enables animation, styling, binding, etc...
	    public static readonly DependencyProperty HealthFontFamilyProperty =
		    DependencyProperty.Register("HealthFontFamily", typeof(FontFamily), typeof(PlayerHUD),
		                                new PropertyMetadata(new FontFamily("Arial"), FontChanged));

	    private static void FontChanged(DependencyObject obj, DependencyPropertyChangedEventArgs args) {
		    int i = 0;
	    }

	    #endregion

		#region MainBrush

		/// <summary>
		/// Main brush for the control
		/// </summary>
		public Brush MainBrush {
            get { return (Brush) GetValue(MainBrushProperty); }
            set { SetValue(MainBrushProperty, value); }
        }

	    public static readonly DependencyProperty MainBrushProperty =
		    DependencyProperty.Register("MainBrush", typeof(Brush), typeof(PlayerHUD),
		                                new PropertyMetadata(Brushes.DodgerBlue));

        #endregion

        #region Armor

        /// <summary>
        /// Player's armor
        /// </summary>
        public int Armor {
            get { return (int) GetValue(ArmorProperty); }
            set { SetValue(ArmorProperty, value); }
        }

        public static readonly DependencyProperty ArmorProperty =
            DependencyProperty.Register("Armor", typeof(int), typeof(PlayerHUD), new PropertyMetadata(0));

        #endregion

        #region Assists

        /// <summary>
        /// Player's assists quantity
        /// </summary>
        public int Assists {
            get { return (int) GetValue(AssistsProperty); }
            set { SetValue(AssistsProperty, value); }
        }

        public static readonly DependencyProperty AssistsProperty =
            DependencyProperty.Register("Assists", typeof(int), typeof(PlayerHUD), new PropertyMetadata(0));

        #endregion

        #region Deaths

        /// <summary>
        /// Player's deaths quantity
        /// </summary>
        public int Deaths {
            get { return (int) GetValue(DeathsProperty); }
            set { SetValue(DeathsProperty, value); }
        }

        public static readonly DependencyProperty DeathsProperty =
            DependencyProperty.Register("Deaths", typeof(int), typeof(PlayerHUD), new PropertyMetadata(0));

        #endregion

        #region Grenades

        /// <summary>
        /// Grenades
        /// </summary>
        private EquipmentElement[] Grenades {
            get { return (EquipmentElement[]) GetValue(GrenadesProperty); }
            set { SetValue(GrenadesProperty, value); }
        }

        private static readonly DependencyProperty GrenadesProperty =
            DependencyProperty.Register("Grenades", typeof(EquipmentElement[]), typeof(PlayerHUD),
                new PropertyMetadata(new EquipmentElement[] {0, 0, 0, 0}));

        #endregion

        #region Health 

        /// <summary>
        /// Player's health
        /// </summary>
        public int Health {
            get { return (int) GetValue(HealthProperty); }
            set { SetValue(HealthProperty, value); }
        }

	    public static readonly DependencyProperty HealthProperty =
		    DependencyProperty.Register("Health", typeof(int), typeof(PlayerHUD), new PropertyMetadata(0));

	    #endregion

        #region Helmet

        /// <summary>
        /// Defines whether player have a helmet or not
        /// </summary>
        public bool Helmet {
            get { return (bool) GetValue(HelmetProperty); }
            set { SetValue(HelmetProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Helmet.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HelmetProperty =
            DependencyProperty.Register("Helmet", typeof(bool), typeof(PlayerHUD), new PropertyMetadata(false));


        #endregion

        #region DefuseKits
        
        public bool HasDefuseKit {
            get { return (bool)GetValue(HasDefuseKitProperty); }
            set { SetValue(HasDefuseKitProperty, value); }
        }

        // Using a DependencyProperty as the backing store for HasDefuseKit.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty HasDefuseKitProperty =
            DependencyProperty.Register("HasDefuseKit", typeof(bool), typeof(PlayerHUD), new PropertyMetadata(false,
                (o, args) => { },
                (o, value) => {
                    ((PlayerHUD) o).Equipments =
                        (bool) value ? EquipmentElement.DefuseKit : EquipmentElement.Unknown;
                    return value;
                }));

        #endregion

        #region Kills

        /// <summary>
        /// Player's kills quantity
        /// </summary>
        public int Kills {
            get { return (int) GetValue(KillsProperty); }
            set { SetValue(KillsProperty, value); }
        }

        public static readonly DependencyProperty KillsProperty =
            DependencyProperty.Register("Kills", typeof(int), typeof(PlayerHUD), new PropertyMetadata(0));

        #endregion

        #region Money

        /// <summary>
        /// Player's money
        /// </summary>
        public int Money {
            get { return (int) GetValue(MoneyProperty); }
            set { SetValue(MoneyProperty, value); }
        }

	    public static readonly DependencyProperty MoneyProperty =
		    DependencyProperty.Register("Money", typeof(int), typeof(PlayerHUD), new PropertyMetadata(0));

        #endregion

        #region PlayerName

        /// <summary>
        /// Player's name
        /// </summary>
        public string PlayerName {
            get { return (string) GetValue(PlayerNameProperty); }
            set { SetValue(PlayerNameProperty, value); }
        }

        public static readonly DependencyProperty PlayerNameProperty =
            DependencyProperty.Register("PlayerName", typeof(string), typeof(PlayerHUD),
                new PropertyMetadata("Player"));

        #endregion

        #region Primary Weapon

        /// <summary>
        /// Primary weapon
        /// </summary>
        private EquipmentElement Primary {
            get { return (EquipmentElement) GetValue(PrimaryProperty); }
            set { SetValue(PrimaryProperty, value); }
        }

        private static readonly DependencyProperty PrimaryProperty =
            DependencyProperty.Register("Primary", typeof(EquipmentElement), typeof(PlayerHUD),
                new PropertyMetadata(EquipmentElement.Unknown));

        #endregion

        #region Secondary Weapon

        /// <summary>
        /// Secondary weapon
        /// </summary>
        private EquipmentElement Secondary {
            get { return (EquipmentElement) GetValue(SecondaryProperty); }
            set { SetValue(SecondaryProperty, value); }
        }

        private static readonly DependencyProperty SecondaryProperty =
            DependencyProperty.Register("Secondary", typeof(EquipmentElement), typeof(PlayerHUD),
                new PropertyMetadata(EquipmentElement.Unknown));

        #endregion

        #region Weapons

        /// <summary>
        /// Player's weapons
        /// </summary>
        public IEnumerable<Equipment> Weapons {
            get { return (IEnumerable<Equipment>) GetValue(WeaponsProperty); }
            set { SetValue(WeaponsProperty, value); }
        }

        private int weaponCount;
        private int prevMoney;

        public static readonly DependencyProperty WeaponsProperty =
            DependencyProperty.Register("Weapons", typeof(IEnumerable<Equipment>), typeof(PlayerHUD),
                new PropertyMetadata(new Equipment[0], (o, args) => { },
                    (o, value) => {
                        PlayerHUD player = (PlayerHUD) o;
                        if (player.weaponCount != ((IEnumerable<Equipment>) value).Count()
                            || player.prevMoney != player.Money) {
                            player.prevMoney = player.Money;
                            player.weaponCount = ((IEnumerable<Equipment>) value).Count();
                            int i = 0;
                            player.Primary = EquipmentElement.Unknown;
                            player.Secondary = EquipmentElement.Unknown;
                            player.Equipments = EquipmentElement.Unknown;

                            EquipmentElement[] nades = {
                                EquipmentElement.Unknown,
                                EquipmentElement.Unknown,
                                EquipmentElement.Unknown,
                                EquipmentElement.Unknown
                            };
                            foreach (var weapon in (IEnumerable<Equipment>) value) {
                                switch (weapon.Class) {
                                    case EquipmentClass.Pistol: {
                                        player.Secondary = weapon.Weapon;
                                        break;
                                    }
                                    case EquipmentClass.Heavy:
                                    case EquipmentClass.Rifle:
                                    case EquipmentClass.SMG: {
                                        player.Primary = weapon.Weapon;
                                        break;
                                    }
                                    case EquipmentClass.Equipment: {
                                        if (weapon.Weapon == EquipmentElement.Bomb)
                                            player.Equipments = weapon.Weapon;
                                        break;
                                    }
                                    case EquipmentClass.Grenade: {
                                        nades[i] = weapon.Weapon;
                                        i++;
                                        break;
                                    }
                                }
                            }

                            player.Grenades = nades;
                        }

                        return value;
                    }));

        #endregion

        #region Equipments
        
        /// <summary>
        /// DefuseKit / Bomb
        /// </summary>
        private EquipmentElement Equipments {
            get { return (EquipmentElement) GetValue(EquipmentsProperty); }
            set { SetValue(EquipmentsProperty, value); }
        }

        private static readonly DependencyProperty EquipmentsProperty =
            DependencyProperty.Register("Equipments", typeof(EquipmentElement), typeof(PlayerHUD),
                new PropertyMetadata(EquipmentElement.Unknown));

        #endregion
    }

    #region Convertors

    public class ArmorConvertor : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            ResourceDictionary d = new ResourceDictionary();
            d.Source = new Uri("pack://application:,,,/CustomControls;component/dict/weapons.xaml");
            ImageSource img = null;
            if ((int)value > 0)
                img = (ImageSource)d["armor"];
            return img;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value != null)
                return 1;
            return 0;
        }
    }

    public class MoneyConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            return (int)value + " $";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            return int.Parse((string)value);
        }
    }

    public class HelmetConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if ((bool)value) {
                ResourceDictionary d = new ResourceDictionary();
                d.Source = new Uri("pack://application:,,,/CustomControls;component/dict/weapons.xaml");
                return (ImageSource)d["helmet"];
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value != null)
                return true;
            return false;
        }
    }

    public class WeaponConverter : IValueConverter {
        public ResourceDictionary dictionary;

        public WeaponConverter() {
            dictionary = new ResourceDictionary();
            dictionary.Source = new Uri("pack://application:,,,/CustomControls;component/dict/weapons.xaml");
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            int v;
            if (parameter != null)
                v = (int) ((EquipmentElement[]) value)[int.Parse((string) parameter)];
            else v = (int) value;
            switch (v) {
                case (int)EquipmentElement.AK47:
                    return (ImageSource)dictionary["ak47"];

                case (int)EquipmentElement.AUG:
                    return (ImageSource)dictionary["aug"];

                case (int)EquipmentElement.AWP:
                    return (ImageSource)dictionary["awp"];

                case (int)EquipmentElement.Bizon:
                    return (ImageSource)dictionary["bizon"];

                case (int)EquipmentElement.Bomb:
                    return (ImageSource) dictionary["bomb"];

                case (int)EquipmentElement.CZ:
                    return (ImageSource)dictionary["cz75"];

                case (int)EquipmentElement.Deagle:
                    return (ImageSource)dictionary["deagle"];

                case (int)EquipmentElement.Decoy:
                    return (ImageSource)dictionary["decoy"];

                case (int)EquipmentElement.DefuseKit:
                    return (ImageSource) dictionary["defusekit"];

                case (int)EquipmentElement.DualBarettas:
                    return (ImageSource)dictionary["elite"];

                case (int)EquipmentElement.Famas:
                    return (ImageSource)dictionary["famas"];

                case (int)EquipmentElement.FiveSeven:
                    return (ImageSource)dictionary["fiveseven"];

                case (int)EquipmentElement.Flash:
                    return (ImageSource)dictionary["flashbang"];

                case (int)EquipmentElement.G3SG1:
                    return (ImageSource)dictionary["g3sg1"];

                case (int)EquipmentElement.Gallil:
                    return (ImageSource)dictionary["galil"];

                case (int)EquipmentElement.Glock:
                    return (ImageSource)dictionary["glock"];

                case (int)EquipmentElement.HE:
                    return (ImageSource)dictionary["hegrenade"];

                case (int)EquipmentElement.Incendiary:
                    return (ImageSource)dictionary["incendiary"];

                case (int)EquipmentElement.M4A4:
                    return (ImageSource)dictionary["m4a1"];

                case (int)EquipmentElement.M4A1:
                    return (ImageSource)dictionary["m4a1s"];

                case (int)EquipmentElement.M249:
                    return (ImageSource)dictionary["m249"];

                case (int)EquipmentElement.Mac10:
                    return (ImageSource)dictionary["mac10"];

                case (int)EquipmentElement.Swag7:
                    return (ImageSource)dictionary["mag7"];

                case (int)EquipmentElement.Molotov:
                    return (ImageSource)dictionary["molotov"];

                case (int)EquipmentElement.MP7:
                    return (ImageSource)dictionary["mp7"];

                case (int)EquipmentElement.MP9:
                    return (ImageSource)dictionary["mp9"];

                case (int)EquipmentElement.Negev:
                    return (ImageSource)dictionary["negev"];

                case (int)EquipmentElement.Nova:
                    return (ImageSource)dictionary["nova"];

                case (int)EquipmentElement.P90:
                    return (ImageSource)dictionary["p90"];

                case (int)EquipmentElement.P250:
                    return (ImageSource)dictionary["p250"];

                case (int)EquipmentElement.SawedOff:
                    return (ImageSource)dictionary["sawedoff"];

                case (int)EquipmentElement.Scar20:
                    return (ImageSource)dictionary["scar"];

                case (int)EquipmentElement.SG556:
                    return (ImageSource)dictionary["sg553"];

                case (int)EquipmentElement.Smoke:
                    return (ImageSource)dictionary["smoke"];

                case (int)EquipmentElement.Scout:
                    return (ImageSource)dictionary["ssg08"];

                case (int)EquipmentElement.Tec9:
                    return (ImageSource)dictionary["tec9"];

                case (int)EquipmentElement.UMP:
                    return (ImageSource)dictionary["ump45"];

                case (int)EquipmentElement.P2000:
                case (int)EquipmentElement.USP:
                    return (ImageSource)dictionary["usp"];

                case (int)EquipmentElement.XM1014:
                    return (ImageSource)dictionary["xm1014"];
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }

	public class HelmetSize:IValueConverter {
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
			Thickness m = new Thickness(0);
			m.Top = (double) value / 3.3;
			m.Bottom = m.Top * 1.5;
			return m;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
			throw new NotImplementedException();
		}
	}

	#endregion
}

﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using DemoInfo;

namespace CustomControls {
	/// <summary>
	/// Логика взаимодействия для KillLabel.xaml
	/// </summary>
	public partial class KillLabel : UserControl {
		public KillLabel() {
			InitializeComponent();
		}

		private static Brush CTbrush = new SolidColorBrush(new Color {A = 255, R = 69, G = 155, B = 230});
		private static Brush Tbrush = new SolidColorBrush(new Color {A = 255, R = 255, G = 188, B = 64});


		public KillLabel(Player Killer, Player Victim, EquipmentElement Weapon, bool isHeadshot = false) {
			InitializeComponent();
			killerName.Text = Killer.Name;
			killerName.Foreground = Killer.Team == Team.CounterTerrorist ? CTbrush : Tbrush;
			victimName.Text = Victim.Name;
			victimName.Foreground = Victim.Team == Team.CounterTerrorist ? CTbrush : Tbrush;
			WeaponConverter wc = new WeaponConverter();
			this.Weapon.Source = (ImageSource) wc.Convert(Weapon, null, null, null);
			if (isHeadshot) {
				Headshot.Margin = new Thickness(5, 0, 0, 0);
				Headshot.Source = (ImageSource) wc.dictionary["headshot"];
			}
		}
	}
	public class ImagesSize:IValueConverter {
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
			return (double) value * 0.8;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
			throw new NotImplementedException();
		}
	}
}

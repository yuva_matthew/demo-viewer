﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;

namespace CustomControls {
	/// <summary>
	/// Логика взаимодействия для CircleHealthBar.xaml
	/// </summary>
	public partial class CircleHealthBar : UserControl {
		public CircleHealthBar() {
			InitializeComponent();
		}

		#region Brushes

		public Brush ProgressBrush {
			get { return (Brush)GetValue(ProgressBrushProperty); }
			set { SetValue(ProgressBrushProperty, value); }
		}

		// Using a DependencyProperty as the backing store for BorderBrush.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty ProgressBrushProperty =
			DependencyProperty.Register("ProgressBrush", typeof(Brush), typeof(CircleHealthBar),
										new PropertyMetadata(Brushes.DodgerBlue));


		#endregion

		#region Low Health

		public bool LowHP {
			get { return (bool)GetValue(LowHPProperty); }
		}

		// Using a DependencyProperty as the backing store for LowHP.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty LowHPProperty =
			DependencyProperty.Register("LowHP", typeof(bool), typeof(CircleHealthBar), new PropertyMetadata(true));

		#endregion

		#region Value

		public int Value {
			get { return (int)GetValue(ValueProperty); }
			set { SetValue(ValueProperty, value); }
		}

		// Using a DependencyProperty as the backing store for Value.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty ValueProperty =
			DependencyProperty.Register("Value", typeof(int), typeof(CircleHealthBar), new PropertyMetadata(0, Value_Changed, Coerce_Value));

		private static object Coerce_Value(DependencyObject dependencyObject, object baseValue) {
			if ((int)baseValue > 100) return 100;
			if ((int)baseValue < 0) return 0;
			return baseValue;
		}

		private static void Value_Changed(DependencyObject obj, DependencyPropertyChangedEventArgs args) {
			int newValue = (int)args.NewValue;
			((CircleHealthBar)obj).SetValue(LowHPProperty, newValue < 26);
		}

		#endregion
	}

	public class SizeConverter : IValueConverter {
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
			return (double)value * double.Parse(parameter.ToString(), CultureInfo.InvariantCulture);
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
			throw new NotImplementedException();
		}
	}

	public class AngleConverter : IValueConverter {
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
			return 180.0 + int.Parse(parameter.ToString()) * 1.8 * (int)value;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
			throw new NotImplementedException();
		}
	}
}

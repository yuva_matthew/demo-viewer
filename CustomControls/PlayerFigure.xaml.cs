﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace CustomControls {
    /// <summary>
    /// Логика взаимодействия для PlayerFigure.xaml
    /// </summary>
    public partial class PlayerFigure : Control {
        public PlayerFigure(double width, Brush main, Brush border) {
            InitializeComponent();
            Width = width;
            if (main != null)
                Foreground = main;
            if (border != null)
                BorderBrush = border;
        }

        #region ViewDirection
        
        /// <summary>
        /// View direction of the player
        /// </summary>
        public float ViewDirection {
            get { return (float)GetValue(ViewDirectionProperty); }
            set { SetValue(ViewDirectionProperty, value); }
        }

        public static readonly DependencyProperty ViewDirectionProperty =
            DependencyProperty.Register("ViewDirection", typeof(float), typeof(PlayerFigure), new PropertyMetadata(0.0f));

        #endregion

        private void Control_SizeChanged(object sender, SizeChangedEventArgs e) {
            if (e.HeightChanged)
                Width = e.NewSize.Height;
            else Height = e.NewSize.Width;
        }
    }

    #region Converters

    public class WidthConverter : System.Windows.Data.IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            return (double)value * 0.733;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }

    public class HeightConverter : System.Windows.Data.IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            return (double)value * 0.533;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }

    public class CircleConverter : System.Windows.Data.IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            return (double)value - 2 * (double)value * 0.1;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException();
        }
    }
    
    #endregion

}

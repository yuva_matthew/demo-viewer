﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace CustomControls {
	/// <summary>
	/// Логика взаимодействия для TimeLine.xaml
	/// </summary>
	public partial class TimeLine : UserControl {
		public TimeLine() {
			InitializeComponent();
			IsPaused = true;
		}

		#region TrackBackgroundBrush

		public Brush TrackBackgroundBrush {
			get { return (Brush) GetValue(TrackBackgroundBrushProperty); }
			set { SetValue(TrackBackgroundBrushProperty, value); }
		}

		// Using a DependencyProperty as the backing store for TrackBackgroundBrush.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty TrackBackgroundBrushProperty =
			DependencyProperty.Register("TrackBackgroundBrush", typeof(Brush), typeof(TimeLine),
			                            new PropertyMetadata(new SolidColorBrush(Color.FromArgb(128, 255, 255, 255))));

		#endregion

		#region IsPaused
		
		public bool IsPaused {
			get { return (bool)GetValue(IsPausedProperty); }
			set { SetValue(IsPausedProperty, value); }
		}

		// Using a DependencyProperty as the backing store for IsPaused.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty IsPausedProperty =
			DependencyProperty.Register("IsPaused", typeof(bool), typeof(TimeLine), new PropertyMetadata(true));
		
		#endregion

		#region Value

		public int Value {
			get { return (int) GetValue(ValueProperty); }
			set { SetValue(ValueProperty, value); }
		}

		// Using a DependencyProperty as the backing store for Value.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty ValueProperty =
			DependencyProperty.Register("Value", typeof(int), typeof(TimeLine),
			                            new PropertyMetadata(0, Value_Changed, Value_Coerce));

		private static void Value_Changed(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs args) {
			TimeLine self = (TimeLine) dependencyObject;
			Rectangle track = (Rectangle) self.GetTemplateChild("ActiveTrack");
			Rectangle backTrack = (Rectangle) self.GetTemplateChild("InactiveTrack");
			Grid circle = (Grid) self.GetTemplateChild("Circle");
			if (backTrack != null &&
			    track != null &&
			    circle != null) {
				track.Width = backTrack.ActualWidth * (Convert.ToDouble(args.NewValue) / self.MaxValue);
				circle.Margin = new Thickness(track.Width + circle.ActualWidth/2.0, 0, 0, 0);
			}
		}

		private static object Value_Coerce(DependencyObject dependencyObject, object baseValue) {
			int value = (int) baseValue;
			TimeLine self = (TimeLine) dependencyObject;
			if (value <= 0)
				return 0;
			if (value > self.MaxValue)
				return self.MaxValue;
			return value;
		}

		#endregion

		#region MaxValue

		public int MaxValue {
			get { return (int) GetValue(MaxValueProperty); }
			set { SetValue(MaxValueProperty, value); }
		}

		// Using a DependencyProperty as the backing store for MaxValue.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty MaxValueProperty =
			DependencyProperty.Register("MaxValue", typeof(int), typeof(TimeLine),
			                            new PropertyMetadata(100, (sender, args) => { }, MaxValue_Coerce));

		private static object MaxValue_Coerce(DependencyObject dependencyObject, object baseValue) {
			int value = (int) baseValue;
			TimeLine self = (TimeLine) dependencyObject;
			if (value < 0)
				return 0;
			if (value < self.Value)
				return self.Value;
			return value;
		}

		#endregion

		#region PlayButton_Click

		public static readonly RoutedEvent PlayButtonClickEvent = EventManager.RegisterRoutedEvent("PlayButtonClick", RoutingStrategy.Direct, typeof(RoutedEventHandler), typeof(TimeLine));

		public event RoutedEventHandler PlayButtonClick {
			add {
				base.AddHandler(PlayButtonClickEvent, value);
			}
			remove {
				base.RemoveHandler(PlayButtonClickEvent, value);
			}
		}

		private void PlayButton_Click(object sender, RoutedEventArgs e) {
			IsPaused = !IsPaused;
			base.RaiseEvent(new RoutedEventArgs(PlayButtonClickEvent, this));
		}


		#endregion

		#region Circle_Move
		
		public bool CanMove {
			get { return (bool)GetValue(CanMoveProperty); }
			set { SetValue(CanMoveProperty, value); }
		}

		// Using a DependencyProperty as the backing store for CanMove.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty CanMoveProperty =
			DependencyProperty.Register("CanMove", typeof(bool), typeof(TimeLine), new PropertyMetadata(true));
		

		public static readonly RoutedEvent CircleMoveEvent =
			EventManager.RegisterRoutedEvent("CircleMove", RoutingStrategy.Direct, typeof(RoutedEventHandler), typeof(TimeLine));

		public event RoutedEventHandler CircleMove {
			add {
				base.AddHandler(CircleMoveEvent, value);
			}
			remove {
				base.RemoveHandler(CircleMoveEvent, value);
			}
		}
		
		private bool pressed;

		private void Circle_MouseMove(object sender, MouseEventArgs e) {
			if (pressed && CanMove) {
				Rectangle inactiveTrack = (Rectangle) this.GetTemplateChild("InactiveTrack");
				Point p = e.GetPosition(inactiveTrack);
				Value = (int) (p.X / (inactiveTrack.ActualWidth / MaxValue));
				base.RaiseEvent(new RoutedEventArgs(CircleMoveEvent, this));
			}
		}

		public static readonly RoutedEvent CircleMouseLeftButtonDownEvent =
			EventManager.RegisterRoutedEvent("CircleMouseLeftButtonDown", RoutingStrategy.Direct, typeof(RoutedEventHandler),
			                                 typeof(TimeLine));

		public event RoutedEventHandler CircleMouseLeftButtonDown {
			add {
				base.AddHandler(CircleMouseLeftButtonDownEvent, value);
			}
			remove {
				base.RemoveHandler(CircleMouseLeftButtonDownEvent, value);
			}
		}

		private void Circle_MouseLeftButtonDown(object sender, MouseButtonEventArgs e) {
			if(CanMove) {
				pressed = true;
				base.RaiseEvent(new RoutedEventArgs(CircleMouseLeftButtonDownEvent, this));
			}
		}

		private void Self_MouseLeftButtonUp(object sender, MouseButtonEventArgs e) {
			pressed = false;
		}

		private void Self_MouseLeave(object sender, MouseEventArgs e) {
			pressed = false;
		}

		#endregion
	}
}

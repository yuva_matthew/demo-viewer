﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace CustomControls {
    public partial class PBar : UserControl {
        public PBar() {
            InitializeComponent();
            percent.ForeColor = Color.Black;
            ForeColor = SystemColors.Highlight;
            MinValue = 0;
            MaxValue = 100;
        }

        private double progress;

        public int MinValue { get; set; }
        public int MaxValue { get; set; }
        public double Value {
            get { return progress; }
            set {
                if (value < MinValue)
                    value = MinValue;
                else if (value > MaxValue)
                    value = MaxValue;
                progress = value;
                percent.Text = progress + " %";
                Invalidate();
            }
        }

        protected override void OnPaint(PaintEventArgs e) {
            base.OnPaint(e);
            Brush b = new SolidBrush(Color.FromArgb(205, ForeColor));
            int width = (int) (Value / MaxValue * Width);
            GraphicsPath gp = new GraphicsPath();
            gp.AddRectangle(new Rectangle(0, 0, Width, Height));
            PathGradientBrush path = new PathGradientBrush(gp);
            path.CenterColor = Color.FromArgb(80, Color.White);
            path.SurroundColors = new[] {Color.FromArgb(20, Color.White)};
            e.Graphics.FillRectangle(b, 0, 0, width, Height);
            e.Graphics.FillRectangle(path, 0, 0, width, Height);
            b.Dispose();
            path.Dispose();
        }
        private void PBar_SizeChanged(object sender, EventArgs e) {
            percent.Location = new Point(Width / 2 - 13, Height / 2 - 15 / 2);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DemoInfo;

namespace CustomControls {
	/// <summary>
	/// Логика взаимодействия для GrenadeControl.xaml
	/// </summary>
	public partial class GrenadeControl : UserControl {
		public GrenadeControl() {
			InitializeComponent();
		}

		#region GrenadeType

		public EquipmentElement GrenadeType {
			get { return (EquipmentElement)GetValue(GrenadeTypeProperty); }
			set { SetValue(GrenadeTypeProperty, value); }
		}

		// Using a DependencyProperty as the backing store for GrenadeType.  This enables animation, styling, binding, etc...
		public static readonly DependencyProperty GrenadeTypeProperty =
			DependencyProperty.Register("GrenadeType", typeof(EquipmentElement), typeof(GrenadeControl),
			                            new PropertyMetadata(EquipmentElement.Unknown, Type_Changed));

		private static void Type_Changed(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs args) {
			if ((EquipmentElement) args.NewValue == EquipmentElement.Smoke) {
				GrenadeControl self = (GrenadeControl) dependencyObject;
				Ellipse e = self.hui;
			}
		}

		#endregion

	}
}

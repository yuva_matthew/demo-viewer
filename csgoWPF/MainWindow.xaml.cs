﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using csgoWPF.SourceCode.Maps;
using CustomControls;
using DemoInfo;

namespace csgoWPF {

    /// <summary>
    ///     Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {

        private PlayerHUD[] ct, t;
        private LogWriter log;
        private TextBlock[] names;
        private PlayerFigure[] players;

        private readonly int startTick;

        public MainWindow(string DemoPath, int Tick) {
            this.parser = new DemoParser(File.OpenRead(DemoPath));
            this.startTick = Tick;
            this.InitializeComponent();
        }

        private void Window_Closing(object sender, CancelEventArgs e) {
            if (this.timer != null) {
                this.timer.Stop();
                this.timer.Close();
            }

            this.log.Close();
        }

        private void MapImage_SizeChanged(object sender, SizeChangedEventArgs e) {
            if (this.started) {
                this.mapWidthDelta = this.MapImage.Margin.Left + (this.MapImage.Width - this.MapImage.ActualWidth) / 2;
                this.mapHeightDelta =
                    this.MapImage.Margin.Top + (this.MapImage.Height - this.MapImage.ActualHeight) / 2;
                this.widthConstant = this.MapImage.ActualWidth / this.map.Width;
                this.heightConstant = this.MapImage.ActualHeight / this.map.Height;

                for (var i = 0; i < 5; i++) {
                    // Counter-Terrorists
                    if (this.ct[i].Tag != null) {
                        var left = this.mapWidthDelta +
                                   (((Player) this.ct[i].Tag).Position.X - this.map.PosX) * this.widthConstant -
                                   this.players[0].ActualWidth / 2;
                        var up = this.mapHeightDelta +
                                 (this.map.PosY - ((Player) this.ct[i].Tag).Position.Y) * this.heightConstant -
                                 this.players[0].ActualHeight / 2;
                        this.players[i].Margin = new Thickness(left, up, 0, 0);
                        this.names[i].Margin = new Thickness(left - this.names[i].ActualWidth, up, 0, 0);
                        this.players[i].ViewDirection = -((Player) this.ct[i].Tag).ViewDirectionX;
                    }

                    // Terrorists
                    if (this.t[i].Tag != null) {
                        var left = this.mapWidthDelta +
                                   (((Player) this.t[i].Tag).Position.X - this.map.PosX) * this.widthConstant -
                                   this.players[0].ActualWidth / 2;
                        var up = this.mapHeightDelta +
                                 (this.map.PosY - ((Player) this.t[i].Tag).Position.Y) * this.heightConstant -
                                 this.players[0].ActualHeight / 2;
                        this.players[i + 5].Margin = new Thickness(left, up, 0, 0);
                        this.names[i + 5].Margin = new Thickness(left, up + this.players[i].ActualHeight, 0, 0);
                        this.players[i + 5].ViewDirection = -((Player) this.t[i].Tag).ViewDirectionX;
                    }
                }
            }
        }

        private void Window_Initialized(object sender, EventArgs e) {
            #region Initializing

            this.ct = new[] {this.CT_1, this.CT_2, this.CT_3, this.CT_4, this.CT_5};
            this.t = new[] {this.T_1, this.T_2, this.T_3, this.T_4, this.T_5};

            // T Colors
            var tMainColor = new SolidColorBrush(Color.FromArgb(255, 255, 188, 64));
            var tBorderColor = new SolidColorBrush(Color.FromArgb(154, 255, 200, 80));

            // CT Colors
            var ctMainColor = new SolidColorBrush(Color.FromArgb(255, 48, 54, 74));
            var ctBorderColor = new SolidColorBrush(Color.FromArgb(154, 196, 208, 238));

            this.players = new[] {
                // CT
                new PlayerFigure(25, ctMainColor, ctBorderColor),
                new PlayerFigure(25, ctMainColor, ctBorderColor),
                new PlayerFigure(25, ctMainColor, ctBorderColor),
                new PlayerFigure(25, ctMainColor, ctBorderColor),
                new PlayerFigure(25, ctMainColor, ctBorderColor),
                // T
                new PlayerFigure(25, tMainColor, tBorderColor),
                new PlayerFigure(25, tMainColor, tBorderColor),
                new PlayerFigure(25, tMainColor, tBorderColor),
                new PlayerFigure(25, tMainColor, tBorderColor),
                new PlayerFigure(25, tMainColor, tBorderColor)
            };
            this.names = new TextBlock[10];

            for (var i = 0; i < 10; i++) {
                this.names[i] = new TextBlock {
                    Foreground = new SolidColorBrush(Color.FromArgb(255, 240, 248, 255)),
                    Background = new SolidColorBrush(Color.FromArgb(102, 16, 16, 22)),
                    FontFamily = (FontFamily) Application.Current.Resources["GoogleSans"],
                    FontSize = 14
                };
                this.canvas.Children.Add(this.players[i]);
                this.canvas.Children.Add(this.names[i]);
            }

            #endregion

            #region Parser

            this.parser.ParseHeader();

            switch (this.parser.Map) {
                case "de_cache": {
                    this.MapImage.Source = new BitmapImage(new Uri(@"images\cache.png", UriKind.Relative));
                    this.map = new Cache();
                    break;
                }
                case "de_mirage": {
                    this.MapImage.Source = new BitmapImage(new Uri(@"images\mirage.png", UriKind.Relative));
                    this.map = new Mirage();
                    break;
                }
                case "de_inferno": {
                    this.MapImage.Source = new BitmapImage(new Uri(@"images\inferno.png", UriKind.Relative));
                    this.map = new Inferno();
                    break;
                }
                case "de_dust2": {
                    this.MapImage.Source = new BitmapImage(new Uri(@"images\dust2.png", UriKind.Relative));
                    this.map = new Dust2();
                    break;
                }
                case "de_cbble": {
                    this.MapImage.Source = new BitmapImage(new Uri(@"images\cobble.png", UriKind.Relative));
                    this.map = new Cbble();
                    break;
                }
                case "de_train": {
                    this.MapImage.Source = new BitmapImage(new Uri(@"images\train.png", UriKind.Relative));
                    this.map = new Train();
                    break;
                }
                case "de_overpass": {
                    this.MapImage.Source = new BitmapImage(new Uri(@"images\overpass.png", UriKind.Relative));
                    this.map = new Overpass();
                    break;
                }
                case "de_nuke": {
                    this.MapImage.Source = new BitmapImage(new Uri(@"images\nuke.png", UriKind.Relative));
                    this.map = new Nuke();
                    break;
                }
            }

            if (this.map == null) {
                MessageBox.Show("Данная карта не поддерживается", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                this.Close();
            }

            var logName = this.map.Name + '_' +
                          DateTime.Now.ToString(CultureInfo.InvariantCulture).Replace('/', '.').Replace(':', '_') +
                          ".txt";
            this.log = new LogWriter(logName);
            this.Title = this.map.Name;

            #endregion

            while (this.startTick != this.parser.CurrentTick) this.parser.ParseNextTick();
            this.started = true;
            this.round = false;
        }

        private void timeLine_PlayButtonClick(object sender, RoutedEventArgs e) {
            this.timer.Stop();
            if (!this.timeLine.IsPaused) this.timer.Start();
        }

        private void Content_Loaded(object sender, EventArgs e) {
            var square = this.MapImage.ActualWidth < this.MapImage.ActualHeight
                             ? this.MapImage.ActualWidth
                             : this.MapImage.ActualHeight;
            this.mapWidthDelta = this.MapImage.Margin.Left + (this.MapImage.Width - square) / 2;
            this.mapHeightDelta = this.MapImage.Margin.Top + (this.MapImage.Height - square) / 2;
            this.timeLine.CanMove = false;
            this.timeLine.MaxValue = this.parser.Header.PlaybackTicks;
            this.timeLine.Value = this.startTick;
            try {
                this.Begin();
            }
            catch (Exception exc) {
                this.log.Write(exc);
                MessageBox.Show(exc.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                this.Close();
            }
        }

    }

    public class SizeConverter : IValueConverter {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) =>
            (double) value - 40;

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) =>
            throw new NotImplementedException();

    }

}
﻿using System;
using System.Collections;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using DemoInfo;

namespace csgoWPF {

    /// <summary>
    ///     Логика взаимодействия для MenuWindow.xaml
    /// </summary>
    public partial class MenuWindow : Page {

        /// <summary>
        ///     Parser to gather information from demo
        /// </summary>
        private readonly DemoParser Parser;

        /// <summary>
        ///     Path to the demo file
        /// </summary>
        private readonly string Path;

        /// <summary>
        ///     The reason of the round end
        /// </summary>
        private RoundEndReason reason;

        private double roundWidth;

        /// <summary>
        ///     Start time of the round
        /// </summary>
        private int startTime;

        /// <summary>
        ///     Thread that parses demo
        /// </summary>
        private Thread thread;

        /// <summary>
        ///     The winner of a round
        /// </summary>
        private Team winner;

        public MenuWindow(string path) {
            this.InitializeComponent();
            this.Parser = new DemoParser(File.OpenRead(path));
            this.Path = path;
        }

        private void self_Loaded(object sender, RoutedEventArgs e) {
            this.Visibility = Visibility.Collapsed;
            this.roundWidth = this.ActualWidth / 30.0;
            this.Parser.ParseHeader();

            switch (this.Parser.Map) {
                case "de_cache": {
                    this.MapImage.Source = new BitmapImage(new Uri(@"images\cache.png", UriKind.Relative));
                    this.MapName.Text = "CACHE";
                    break;
                }
                case "de_mirage": {
                    this.MapImage.Source = new BitmapImage(new Uri(@"images\mirage.png", UriKind.Relative));
                    this.MapName.Text = "MIRAGE";
                    break;
                }
                case "de_inferno": {
                    this.MapImage.Source = new BitmapImage(new Uri(@"images\inferno.png", UriKind.Relative));
                    this.MapName.Text = "INFERNO";
                    break;
                }
                case "de_dust2": {
                    this.MapImage.Source = new BitmapImage(new Uri(@"images\dust2.png", UriKind.Relative));
                    this.MapName.Text = "DUST 2";
                    break;
                }
                case "de_cbble": {
                    this.MapImage.Source = new BitmapImage(new Uri(@"images\cobble.png", UriKind.Relative));
                    this.MapName.Text = "COBBLESTONE";
                    break;
                }
                case "de_train": {
                    this.MapImage.Source = new BitmapImage(new Uri(@"images\train.png", UriKind.Relative));
                    this.MapName.Text = "TRAIN";
                    break;
                }
                case "de_overpass": {
                    this.MapImage.Source = new BitmapImage(new Uri(@"images\overpass.png", UriKind.Relative));
                    this.MapName.Text = "OVERPASS";
                    break;
                }
                case "de_nuke": {
                    this.MapImage.Source = new BitmapImage(new Uri(@"images\nuke.png", UriKind.Relative));
                    this.MapName.Text = "NUKE";
                    break;
                }
                default:
                    MessageBox.Show("Данная карта не поддерживается", "Ошибка", MessageBoxButton.OK,
                                    MessageBoxImage.Error);
                    this.NavigationService?.GoBack();
                    return;
            }

            this.Visibility = Visibility.Visible;

            Player[] team1 = new Player[5],
                     team2 = new Player[5];
            int ct = 0,
                t = 0;
            var warmup = true;

            // When the warmup is over
            this.Parser.MatchStarted += (ss, ee) => {
                foreach (var player in this.Parser.PlayingParticipants)
                    if (player.Team == Team.CounterTerrorist) {
                        team1[ct] = player;
                        ct++;
                    }
                    else {
                        team2[t] = player;
                        t++;
                    }

                warmup = false;
                this.Dispatcher.Invoke(() => {
                    this.firstTeamName.Header = this.Parser.CTClanName == "" ? "Team 1" : this.Parser.CTClanName;
                    this.secondTeamName.Header = this.Parser.TClanName == "" ? "Team 2" : this.Parser.TClanName;
                    this.CTScore.Text = this.TScore.Text = "0";
                    this.firstTeam.ItemsSource = team1;
                    this.secondTeam.ItemsSource = team2;
                });
                this.startTime = this.Parser.CurrentTick;
            };

            while (warmup)
                if (!this.Parser.ParseNextTick()) {
                    MessageBox.Show(
                        "Невозможно просмотреть запись:\nМатч не начался, т.к. не все участники смогли подключиться к серверу.",
                        "Ошибка", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    break;
                }

            #region Bullshit

            // Every time round officially ends
            this.Parser.RoundOfficiallyEnd += this.RoundOfficiallyEnd;

            // Every time round ends
            this.Parser.RoundEnd += (ss, ee) => {
                this.winner = ee.Winner;
                switch (ee.Message.Substring(13)) {
                    case "CTs_Win":
                        this.reason = RoundEndReason.CTWin;
                        break;
                    case "Terrorists_Win":
                        this.reason = RoundEndReason.TerroristWin;
                        break;
                    case "Bomb_Defused":
                        this.reason = RoundEndReason.BombDefused;
                        break;
                    case "Target_Bombed":
                        this.reason = RoundEndReason.TargetBombed;
                        break;
                    case "Target_Saved":
                        this.reason = RoundEndReason.TargetSaved;
                        break;
                }

                ct = 0; // Quantity of rounds won by CT
                t = 0;  // Quantity of rounds won by T

                #region Updating score

                this.CTScore.Dispatcher.Invoke(() => { ct = int.Parse(this.CTScore.Text); });
                this.TScore.Dispatcher.Invoke(() => { t = int.Parse(this.TScore.Text); });
                if (ct + t < 15) {
                    if (ee.Winner == Team.CounterTerrorist)
                        this.CTScore.Dispatcher.InvokeAsync(() => { this.CTScore.Text = (ct + 1).ToString(); });
                    else
                        this.TScore.Dispatcher.InvokeAsync(() => { this.TScore.Text = (t + 1).ToString(); });
                }
                else {
                    if (ee.Winner == Team.Terrorist)
                        this.CTScore.Dispatcher.InvokeAsync(() => { this.CTScore.Text = (ct + 1).ToString(); });
                    else
                        this.TScore.Dispatcher.InvokeAsync(() => { this.TScore.Text = (t + 1).ToString(); });
                }

                #endregion

                Array.Sort((Player[]) this.firstTeam.ItemsSource,
                           (p1, p2) => {
                               return -new CaseInsensitiveComparer().Compare(p1.AdditionaInformations.Score,
                                                                             p2.AdditionaInformations.Score);
                           });
                Array.Sort((Player[]) this.secondTeam.ItemsSource,
                           (p1, p2) => {
                               return -new CaseInsensitiveComparer().Compare(p1.AdditionaInformations.Score,
                                                                             p2.AdditionaInformations.Score);
                           });
                this.Dispatcher.BeginInvoke(new Action(() => {
                    this.firstTeam.Items.Refresh();
                    this.secondTeam.Items.Refresh();
                }), DispatcherPriority.Background);
            };

            // Every time player dies

            this.Parser.WinPanelMatch += (ss, ee) => {
                this.Dispatcher.InvokeAsync(() => this.NavigationService.StopLoading());
                this.RoundOfficiallyEnd(ss, null);
            };

            this.thread = new Thread(() => {
                this.Parser.ParseToEnd();
                this.Parser.Dispose();
            });
            this.thread.Start();

            #endregion
        }

        private void RoundOfficiallyEnd(object sender, RoundOfficiallyEndedEventArgs roundOfficiallyEndedEventArgs) {
            this.RoundList.Dispatcher.Invoke(() => {
                Brush brush;
                if (this.Parser.TScore + this.Parser.CTScore <= 15) {
                    if (this.winner == Team.Terrorist)
                        brush = new SolidColorBrush(Color.FromArgb(255, 255, 188, 64));
                    else
                        brush = new SolidColorBrush(Color.FromArgb(255, 69, 155, 230));
                }
                else {
                    if (this.winner == Team.CounterTerrorist)
                        brush = new SolidColorBrush(Color.FromArgb(255, 255, 188, 64));
                    else
                        brush = new SolidColorBrush(Color.FromArgb(255, 69, 155, 230));
                }


                var btn = new Button {
                    Foreground = brush,
                    BorderThickness = new Thickness(0),
                    Margin = new Thickness(5, 5, 5, 0),
                    ToolTip = "Round #" + (this.Parser.CTScore + this.Parser.TScore),
                    Tag = this.startTime,
                    Height = this.roundWidth,
                    Width = this.roundWidth
                };
                switch (this.reason) {
                    case RoundEndReason.CTWin:
                    case RoundEndReason.TerroristWin:
                        btn.Template = (ControlTemplate) this.Resources["Eliminated"];
                        break;
                    case RoundEndReason.BombDefused:
                    case RoundEndReason.TargetBombed:
                        btn.Template = (ControlTemplate) this.Resources["Bomb"];
                        break;
                    case RoundEndReason.TargetSaved:
                        btn.Template = (ControlTemplate) this.Resources["Time"];
                        break;
                }

                btn.Click += this.button_Click;
                this.RoundList.Children.Add(btn);
                this.startTime = this.Parser.CurrentTick;
            });
        }

        private void self_SizeChanged(object sender, SizeChangedEventArgs e) {
            var teamHeight = this.firstTeam.ActualHeight;
            var rowHeight = this.firstTeam.RowHeight;
            // Filling all space with rows
            this.firstTeam.RowHeight = this.secondTeam.RowHeight = teamHeight * 0.16;
            this.firstTeam.ColumnHeaderHeight = this.secondTeam.ColumnHeaderHeight = teamHeight * 0.2;
            // Making padding for text
            if (!double.IsNaN(rowHeight)) {
                rowHeight = this.firstTeam.RowHeight / rowHeight;
                var padding = new Thickness(5, this.firstTeam.Padding.Top * rowHeight,
                                            5, this.firstTeam.Padding.Bottom * rowHeight);
                this.firstTeam.Padding = this.secondTeam.Padding = padding;
            }
        }

        private void back_Click(object sender, RoutedEventArgs e) {
            this.thread.Abort();
            this.NavigationService?.GoBack();
        }

        private void button_Click(object sender, RoutedEventArgs routedEventArgs) {
            this.thread.Abort();
            var button = (Button) sender;
            var window = new MainWindow(this.Path, (int) button.Tag);
            if (Application.Current.MainWindow != null)
                Application.Current.MainWindow.Visibility = Visibility.Hidden;
            window.ShowDialog();
            if (Application.Current.MainWindow != null)
                Application.Current.MainWindow.Visibility = Visibility.Visible;
        }

        private void self_Unloaded(object sender, RoutedEventArgs e) {
            this.Parser.Dispose();
        }

    }

}
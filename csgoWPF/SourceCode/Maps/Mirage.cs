﻿namespace csgoWPF.SourceCode.Maps {

    public class Mirage : Map {

        public Mirage() {
            this.Name = "de_mirage";
            this.PosX = -3230;
            this.PosY = 1713;
            this.Scale = 5;
        }

    }

}
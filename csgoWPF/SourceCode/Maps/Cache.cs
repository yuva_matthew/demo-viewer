﻿namespace csgoWPF.SourceCode.Maps {

    public class Cache : Map {

        public Cache() {
            this.Name = "de_cache";
            this.PosX = -2000;
            this.PosY = 3250;
            this.Scale = 5.5;
        }

    }

}
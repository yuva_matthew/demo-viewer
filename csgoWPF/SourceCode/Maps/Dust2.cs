﻿namespace csgoWPF.SourceCode.Maps {

    public class Dust2 : Map {

        public Dust2() {
            this.Name = "de_dust2";
            this.PosX = -2476;
            this.PosY = 3239;
            this.Scale = 4.4;
        }

    }

}
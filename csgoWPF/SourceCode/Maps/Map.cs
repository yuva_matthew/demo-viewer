﻿namespace csgoWPF.SourceCode.Maps {

    public class Map {

        public string Name { get; set; }

        public int PosX { get; set; }

        public int PosY { get; set; }

        public double Scale { get; set; }

        public double Width => 1024 * this.Scale;
        public double Height => 1024 * this.Scale;

    }

}
﻿namespace csgoWPF.SourceCode.Maps {

    public class Inferno : Map {

        public Inferno() {
            this.Name = "de_inferno";
            this.PosX = -2087;
            this.PosY = 3870;
            this.Scale = 4.9;
        }

    }

}
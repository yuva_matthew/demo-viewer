﻿namespace csgoWPF.SourceCode.Maps {

    public class Overpass : Map {

        public Overpass() {
            this.Name = "de_overpass";
            this.PosX = -4831;
            this.PosY = 1781;
            this.Scale = 5.2;
        }

    }

}
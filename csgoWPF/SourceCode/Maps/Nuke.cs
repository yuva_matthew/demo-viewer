﻿namespace csgoWPF.SourceCode.Maps {

    public class Nuke : Map {

        public Nuke() {
            this.Name = "de_nuke";
            this.PosX = -3453;
            this.PosY = 2887;
            this.Scale = 7;
        }

    }

}
﻿using System;
using System.Collections.Generic;
using System.Timers;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Animation;
using csgoWPF.SourceCode.Maps;
using CustomControls;
using DemoInfo;

namespace csgoWPF {

    public partial class MainWindow {

        /// <summary>
        ///     Is this the 2nd half of the game?
        /// </summary>
        private bool half = false;

        /// <summary>
        ///     Current map
        /// </summary>
        private Map map;

        private readonly DemoParser parser;

        /// <summary>
        ///     Bomb planted?
        /// </summary>
        private bool planted;

        /// <summary>
        ///     Round started?
        /// </summary>
        private bool round;

        /// <summary>
        ///     Match started?
        /// </summary>
        private bool started;

        /// <summary>
        ///     Start time of the round
        /// </summary>
        private float startTime;

        private Timer timer;


        public void Begin() {
            this.log.Write("Матч начался!");

            #region Parser Events

            this.parser.TickDone += this.ParserOnTickDone;

            // Bomb Events 
            this.parser.BombPlanted += this.Parser_BombPlanted;
            this.parser.BombDefused += this.Parser_BombDefused;
            this.parser.BombExploded += this.Parser_BombExploded;
            //-------------------------------------------

            // Player Events 
            this.parser.PlayerHurt += this.Parser_PlayerHurt;
            this.parser.PlayerKilled += this.Parser_PlayerKilled;
            //-------------------------------------------

            // Match Events 
            this.parser.FreezetimeEnded += this.Parser_FreezetimeEnded;
            this.parser.WinPanelMatch += this.Parser_WinPanelMatch;
            //-------------------------------------------

            // Round Events 
            this.parser.RoundStart += this.Parser_RoundStart;
            this.parser.RoundEnd += this.Parser_RoundEnd;
            this.parser.RoundOfficiallyEnd += this.Parser_RoundOfficiallyEnd;
            //-------------------------------------------

            #endregion

            #region Timer

            this.timer = new Timer(500.0 / this.parser.TickRate);
            this.timer.Elapsed += (s, e) => {
                this.timer.Stop();
                if (this.parser.ParseNextTick()) this.timer.Start();
            };

            #endregion

            this.GameState.Text = string.Format("Round #{0}", this.parser.CTScore + this.parser.TScore + 1);
            this.TimeBlock.Text = "";
            this.CT_Rounds.Text = this.parser.CTScore.ToString();
            this.T_Rounds.Text = this.parser.TScore.ToString();
            int t_count = 0,
                ct_count = 0;
            foreach (var player in this.parser.PlayingParticipants)
                switch (player.Team) {
                    case Team.CounterTerrorist:
                        this.BindingPlayer(player, player.Team, ref ct_count);
                        break;
                    case Team.Terrorist:
                        this.BindingPlayer(player, player.Team, ref t_count);
                        break;
                }

            this.timeLine.IsPaused = false;
            this.timer.Start();
        }

        private void ParserOnTickDone(object sender, TickDoneEventArgs e) {
            var expressions = new List<BindingExpression>();

            for (var i = 0; i < 5; i++) {
                expressions.Add(this.t[i].GetBindingExpression(PlayerHUD.ArmorProperty));
                expressions.Add(this.t[i].GetBindingExpression(PlayerHUD.HelmetProperty));
                expressions.Add(this.t[i].GetBindingExpression(PlayerHUD.MoneyProperty));
                expressions.Add(this.t[i].GetBindingExpression(PlayerHUD.WeaponsProperty));

                expressions.Add(this.ct[i].GetBindingExpression(PlayerHUD.ArmorProperty));
                expressions.Add(this.ct[i].GetBindingExpression(PlayerHUD.HasDefuseKitProperty));
                expressions.Add(this.ct[i].GetBindingExpression(PlayerHUD.HelmetProperty));
                expressions.Add(this.ct[i].GetBindingExpression(PlayerHUD.MoneyProperty));
                expressions.Add(this.ct[i].GetBindingExpression(PlayerHUD.WeaponsProperty));
            }

            this.Dispatcher.InvokeAsync(() => {
                this.timeLine.Value++;
                if (this.round) {
                    int time;
                    if (this.planted) {
                        time = 40 - (int) (this.parser.CurrentTime - this.startTime);
                        this.TimeBlock.Text = $"{time}";
                    }
                    else {
                        time = 115 - (int) (this.parser.CurrentTime - this.startTime);
                        this.TimeBlock.Text = $"{time / 60:#0}:{time % 60:00}";
                    }
                }

                for (var i = 0; i < 5; i++) {
                    // Counter-Terrorists Positioning
                    if (this.ct[i].Tag != null)
                        if (((Player) this.ct[i].Tag).IsAlive) {
                            // LEFT MARGIN 
                            var left = this.mapWidthDelta +
                                       (((Player) this.ct[i].Tag).Position.X - this.map.PosX) * this.widthConstant -
                                       this.players[0].ActualWidth / 2;
                            // TOP MARGIN 
                            var up = this.mapHeightDelta +
                                     (this.map.PosY - ((Player) this.ct[i].Tag).Position.Y) * this.heightConstant -
                                     this.players[0].ActualHeight / 2;

                            this.players[i].Margin = new Thickness(left, up, 0, 0);
                            // VIEW DIRECTION
                            this.players[i].ViewDirection = -((Player) this.ct[i].Tag).ViewDirectionX;

                            // NAME POSITIONING
                            var alpha = this.players[i].ViewDirection;
                            if (alpha < -180)
                                alpha = 360.0f - Math.Abs(this.players[i].ViewDirection);
                            else if (alpha > 180)
                                alpha = -360.0f + Math.Abs(this.players[i].ViewDirection);
                            var angle = ((Player) this.ct[i].Tag).ViewDirectionX * (float) Math.PI / 180;
                            var delta = 0.5 * this.players[i].ActualHeight - 0.5 * this.names[i].ActualHeight;
                            if (alpha < 90 && alpha > -90)
                                this.names[i].Margin = new Thickness(left - Math.Cos(angle) * this.names[i].ActualWidth,
                                                                     up + Math.Sin(angle) * this.names[i].ActualHeight +
                                                                     delta,
                                                                     0,
                                                                     0);
                            else
                                this.names[i].Margin = new Thickness(
                                    left - Math.Cos(angle) * this.players[i].ActualWidth,
                                    up + Math.Sin(angle) * this.names[i].ActualHeight + delta,
                                    0,
                                    0);
                        }

                    // Terrorists
                    if (this.t[i].Tag != null)
                        if (((Player) this.t[i].Tag).IsAlive) {
                            var left = this.mapWidthDelta +
                                       (((Player) this.t[i].Tag).Position.X - this.map.PosX) * this.widthConstant -
                                       this.players[0].ActualWidth / 2;
                            var up = this.mapHeightDelta +
                                     (this.map.PosY - ((Player) this.t[i].Tag).Position.Y) * this.heightConstant -
                                     this.players[0].ActualHeight / 2;
                            this.players[i + 5].Margin = new Thickness(left, up, 0, 0);
                            this.players[i + 5].ViewDirection = -((Player) this.t[i].Tag).ViewDirectionX;
                            var alpha = this.players[i + 5].ViewDirection;
                            if (alpha < -180)
                                alpha = 360.0f - Math.Abs(this.players[i + 5].ViewDirection);
                            else if (alpha > 180)
                                alpha = -360.0f + Math.Abs(this.players[i + 5].ViewDirection);
                            var angle = ((Player) this.t[i].Tag).ViewDirectionX * (float) Math.PI / 180;
                            var delta = 0.5 * this.players[i + 5].ActualHeight - 0.5 * this.names[i + 5].ActualHeight;
                            if (alpha < 90 && alpha > -90)
                                this.names[i + 5].Margin = new Thickness(
                                    left - Math.Cos(angle) * this.names[i + 5].ActualWidth,
                                    up + Math.Sin(angle) * this.names[i + 5].ActualHeight +
                                    delta,
                                    0,
                                    0);
                            else
                                this.names[i + 5].Margin = new Thickness(
                                    left - Math.Cos(angle) * this.players[i + 5].ActualWidth,
                                    up + Math.Sin(angle) * this.names[i + 5].ActualHeight +
                                    delta,
                                    0,
                                    0);
                        }
                }

                foreach (var ex in expressions)
                    if (ex != null)
                        ex.UpdateTarget();

                expressions.Clear();
            });
        }

        #region Map Constants

        private double mapWidthDelta;
        private double mapHeightDelta;
        private double widthConstant;
        private double heightConstant;

        #endregion

        #region Round Events

        private void Parser_RoundEnd(object sender, RoundEndedEventArgs e) {
            this.Dispatcher.InvokeAsync(() => {
                var roundCount = int.Parse(this.T_Rounds.Text) + int.Parse(this.CT_Rounds.Text) + 1;
                this.log.Write(string.Format("Раунд #{0} закончился", roundCount));
                if (e.Winner == Team.CounterTerrorist)
                    this.CT_Rounds.Text = (int.Parse(this.CT_Rounds.Text) + 1).ToString();
                else
                    this.T_Rounds.Text = (int.Parse(this.T_Rounds.Text) + 1).ToString();
                this.GameState.Text = "Round #" + (roundCount + 1);
                this.TimeBlock.Text = "";
            });
            this.round = false;
            this.planted = false;
        }

        private void Parser_RoundStart(object sender, RoundStartedEventArgs e) {
            this.Dispatcher.Invoke(() => {
                var roundCount = int.Parse(this.T_Rounds.Text) + int.Parse(this.CT_Rounds.Text) + 1;
                this.log.Write(string.Format("Раунд #{0} начался", roundCount));
                this.BombPlanted.Visibility = Visibility.Hidden;
                for (var i = 0; i < 5; i++) {
                    this.ct[i].Health = 100;
                    this.t[i].Health = 100;
                    this.players[i].Visibility = this.players[i + 5].Visibility = Visibility.Visible;
                    this.names[i].Visibility = this.names[i + 5].Visibility = Visibility.Visible;
                }

                if (roundCount == 16) this.SwappingTeams();
            });
        }

        private void Parser_FreezetimeEnded(object sender, FreezetimeEndedEventArgs freezetimeEndedEventArgs) {
            this.round = true;
            this.startTime = this.parser.CurrentTime;
        }

        private void Parser_RoundOfficiallyEnd(object sender, RoundOfficiallyEndedEventArgs e) {
            this.Dispatcher.InvokeAsync(() => {
                for (var i = 0; i < 5; i++) {
                    this.ct[i].Health = 100;
                    this.t[i].Health = 100;
                }
            });
        }

        #endregion

        #region Player Events

        private int FindPlayer(Player p) {
            for (var i = 0; i < 5; i++) {
                if (this.ct[i].Tag == p)
                    return i;
                if (this.t[i].Tag == p)
                    return i + 5;
            }

            return -1;
        }

        private void Parser_PlayerKilled(object sender, PlayerKilledEventArgs e) {
            this.Dispatcher.InvokeAsync(() => {
                // Display kill notice
                var kl = new KillLabel(e.Killer, e.Victim, e.Weapon.Weapon, e.Headshot) {
                    FontFamily = (FontFamily) Application.Current.Resources["GoogleSans"],
                    FontSize = 24,
                    HorizontalAlignment = HorizontalAlignment.Right
                };
                var b = new Binding("ActualHeight") {
                    Source = this.TimeBlock
                };
                kl.SetBinding(MaxHeightProperty, b);
                this.KillNotes.Children.Add(kl);
                var timer = new Timer(3000);
                timer.Elapsed += (ss, ee) => {
                    timer.Stop();
                    this.Dispatcher.InvokeAsync(() => { this.KillNotes.Children.Remove(kl); });
                    timer.Dispose();
                };
                timer.Start();

                this.log.Write(string.Format("Игрок {0} убил игрока {1}", e.Killer.Name, e.Victim.Name));

                // Deaths counter
                var ix = this.FindPlayer(e.Victim);
                PlayerHUD h;
                if (ix < 0)
                    return;
                h = ix < 5 ? this.ct[ix] : this.t[ix - 5];
                h.Deaths++;
                this.players[ix].Visibility = this.names[ix].Visibility = Visibility.Hidden;

                // Kills counter
                if (e.Killer != null) {
                    ix = this.FindPlayer(e.Killer);
                    if (ix > -1) {
                        h = ix < 5 ? this.ct[ix] : this.t[ix - 5];
                        h.Kills++;
                    }
                }

                // Assists counter
                if (e.Assister != null
                ) {
                    ix = this.FindPlayer(e.Killer);
                    if (ix > -1) {
                        h = ix < 5 ? this.ct[ix] : this.t[ix - 5];
                        h.Assists++;
                    }
                }
            });
        }

        private void Parser_PlayerHurt(object sender, PlayerHurtEventArgs e) {
            this.Dispatcher.InvokeAsync(() => {
                var ix = this.FindPlayer(e.Player);
                if (ix < 0)
                    return;
                var h = ix < 5 ? this.ct[ix] : this.t[ix - 5];
                var a = new Int32Animation(h.Health, e.Health, new Duration(TimeSpan.FromMilliseconds(300)));
                a.EasingFunction = new QuarticEase();
                a.FillBehavior = FillBehavior.Stop;
                a.Completed += (o, args) => { h.Health = (int) a.To; };
                h.BeginAnimation(PlayerHUD.HealthProperty, a, HandoffBehavior.Compose);
            });
        }

        #endregion

        #region Match Events

        private void Parser_WinPanelMatch(object sender, WinPanelMatchEventArgs e) {
            this.started = false;
        }

        private void BindingPlayer(Player p, Team team, ref int teammates) {
            switch (team) {
                case Team.CounterTerrorist: {
                    BindingOperations.ClearAllBindings(this.ct[teammates]);
                    var b = new Binding("Armor");
                    b.Source = p;
                    this.ct[teammates].SetBinding(PlayerHUD.ArmorProperty, b);

                    b = new Binding("HasHelmet");
                    b.Source = p;
                    this.ct[teammates].SetBinding(PlayerHUD.HelmetProperty, b);

                    b = new Binding("Money");
                    b.Source = p;
                    this.ct[teammates].SetBinding(PlayerHUD.MoneyProperty, b);

                    b = new Binding("Weapons");
                    b.Source = p;
                    this.ct[teammates].SetBinding(PlayerHUD.WeaponsProperty, b);

                    b = new Binding("HasDefuseKit");
                    b.Source = p;
                    this.ct[teammates].SetBinding(PlayerHUD.HasDefuseKitProperty, b);

                    this.ct[teammates].Kills = p.AdditionaInformations.Kills;
                    this.ct[teammates].Assists = p.AdditionaInformations.Assists;
                    this.ct[teammates].Deaths = p.AdditionaInformations.Deaths;

                    this.ct[teammates].PlayerName = p.Name;
                    this.ct[teammates].Health = 100;
                    this.names[teammates].Text = p.Name;

                    this.ct[teammates].Tag = p;

                    teammates++;
                    break;
                }
                case Team.Terrorist: {
                    BindingOperations.ClearAllBindings(this.t[teammates]);
                    var b = new Binding("Armor");
                    b.Source = p;
                    this.t[teammates].SetBinding(PlayerHUD.ArmorProperty, b);

                    b = new Binding("HasHelmet");
                    b.Source = p;
                    this.t[teammates].SetBinding(PlayerHUD.HelmetProperty, b);

                    b = new Binding("Money");
                    b.Source = p;
                    this.t[teammates].SetBinding(PlayerHUD.MoneyProperty, b);

                    b = new Binding("Weapons");
                    b.Source = p;
                    this.t[teammates].SetBinding(PlayerHUD.WeaponsProperty, b);

                    this.t[teammates].Kills = p.AdditionaInformations.Kills;
                    this.t[teammates].Assists = p.AdditionaInformations.Assists;
                    this.t[teammates].Deaths = p.AdditionaInformations.Deaths;

                    this.t[teammates].PlayerName = p.Name;
                    this.t[teammates].Health = 100;
                    this.names[teammates + 5].Text = p.Name;

                    this.t[teammates].Tag = p;

                    teammates++;
                    break;
                }
            }
        }

        private void SwappingTeams() {
            var tmp = this.CT_Rounds.Text;
            this.CT_Rounds.Text = this.T_Rounds.Text;
            this.T_Rounds.Text = tmp;

            int ctCount = 0, tCount = 0;
            foreach (var player in this.parser.PlayingParticipants)
                switch (player.Team) {
                    case Team.CounterTerrorist:
                        this.BindingPlayer(player, Team.Terrorist, ref tCount);
                        break;
                    case Team.Terrorist:
                        this.BindingPlayer(player, Team.CounterTerrorist, ref ctCount);
                        break;
                }
        }

        #endregion

        #region Bomb Events

        private void Parser_BombExploded(object sender, BombEventArgs e) => this.Dispatcher.InvokeAsync(() => {
            this.BombPlanted.Visibility = Visibility.Hidden;
            this.planted = false;
        });

        private void Parser_BombDefused(object sender, BombEventArgs e) => this.Dispatcher.InvokeAsync(() => {
            this.log.Write("Спецназ разминировал бомбу");
            this.BombPlanted.Visibility = Visibility.Hidden;
            this.planted = false;
        });

        private void Parser_BombPlanted(object sender, BombEventArgs e) => this.Dispatcher.InvokeAsync(() => {
            if (this.round) {
                var left = this.mapWidthDelta + (e.Player.Position.X - this.map.PosX) * this.widthConstant -
                           this.BombPlanted.ActualWidth / 2.0;
                var up = this.mapHeightDelta + (this.map.PosY - e.Player.Position.Y) * this.heightConstant -
                         this.BombPlanted.ActualHeight / 2;
                this.BombPlanted.Margin = new Thickness(left, up, 0, 0);
                this.BombPlanted.Visibility = Visibility.Visible;
                this.planted = true;
                this.startTime = this.parser.CurrentTime;
                this.GameState.Text = "Bomb Planted";
                this.log.Write(string.Format("Террористы установили бомбу в точке {0}", e.Site));
            }
        });

        #endregion

    }

}
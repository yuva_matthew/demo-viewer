﻿using System;
using System.Globalization;
using System.IO;

namespace csgoWPF {

    public class LogWriter {

        private readonly StreamWriter writer;

        public LogWriter(string path) {
            var dirPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)
                          + @"\Demo Viewer\";

            if (!Directory.Exists(dirPath))
                Directory.CreateDirectory(dirPath);

            this.writer = new StreamWriter(File.OpenWrite(dirPath + path));
        }

        public void Write(string msg) {
            this.writer.WriteLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) + "\t" + msg);
        }

        public void Write(Exception exc) {
            this.writer.WriteLine(DateTime.Now.ToString(CultureInfo.InvariantCulture) + "\t" + exc.Message);
        }

        public void Close() {
            this.writer.Close();
        }

    }

}
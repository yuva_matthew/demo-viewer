﻿using System;
using System.Globalization;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Microsoft.Win32;

namespace csgoWPF {

    /// <summary>
    ///     Логика взаимодействия для StartWindow.xaml
    /// </summary>
    public partial class StartWindow : Page {

        public StartWindow() {
            this.InitializeComponent();
            this.NavigationService?.RemoveBackEntry();
        }

        private void Button_Click(object sender, RoutedEventArgs e) {
            var fileDialog = new OpenFileDialog();
            fileDialog.Multiselect = false;
            fileDialog.AddExtension = true;
            fileDialog.DefaultExt = ".dem";
            fileDialog.Filter = "Demo Files|*.dem";
            fileDialog.FileOk += (ss, ee) => {
                if (!File.Exists(fileDialog.FileName)) {
                    MessageBox.Show("Указанный файл не существует", "Ошибка", MessageBoxButton.OK,
                                    MessageBoxImage.Error);
                    return;
                }

                //MainWindow form = new MainWindow(fileDialog.FileName);
                this.NavigationService.Navigate(new MenuWindow(fileDialog.FileName));
                this.NavigationService.RemoveBackEntry();
            };
            fileDialog.ShowDialog();
        }

    }

    public class CornerRadiusConverter : IValueConverter {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) =>
            (double) value / 2.0;

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) =>
            throw new NotImplementedException();

    }

    public class MarginConverter : IValueConverter {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) =>
            (double) value / 2.0;

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) =>
            throw new NotImplementedException();

    }

}